#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Helpful functions for working with indicators of compromise."""

import hashlib
import html
import ipaddress
import os
import re

import six.moves.urllib as urllib
import werkzeug


def url_host(url):
    """Return the host of the given URL."""
    o = urllib.parse.urlparse(url)
    return o.netloc


def url_from_google_redirect(url):
    """Get the url from the google redirect."""
    new_url = None
    host = url_host(url)
    # I'm not sure if the second case will ever be true, but I put it in for good measure
    if host == 'www.google.com' or host == 'google.com':
        new_url = html.unescape(url)
        google_url_pattern = '.*?q=(http.*?)&'
        matches = re.findall(google_url_pattern, new_url)
        if matches and len(matches) == 1:
            new_url = urllib.parse.unquote(matches[0])
        else:
            google_url_pattern_2 = '.*?url=(http.*?)&'
            matches = re.findall(google_url_pattern_2, new_url)
            if matches and len(matches) == 1:
                new_url = urllib.parse.unquote(matches[0])
            else:
                print('Unable to parse the redirection URL from {}'.format(new_url))
                new_url = None
    else:
        print('The host ({}) for this URL ({}) was not detected as a google host. If this is incorrect, please raise an issue here: https://gitlab.com/fhightower/ioc-utility/.'.format(host, url))
        new_url = None
    return new_url


def url_canonical_form(url):
    """Get the canonical url as described here: https://developers.google.com/safe-browsing/v4/urls-hashing#canonicalization."""
    return werkzeug.url_fix(url)
    # parsed_url = urllib.parse.urlparse(url)
    # # TODO: keep working here!!!
    # # in the URL, percent-escape all characters that are <= ASCII 32, >= 127, "#", or "%". The escapes should use uppercase hex characters.
    # url_path = urllib.parse.unquote(parsed_url.path)
    # # replacing "/./" with "/"
    # url_path = url_path.replace('/./', '/')
    # # removing "/../" along with the preceding path component
    # url_path = re.sub('[^\/]*\/\.\.', '', url_path)
    # # replace runs of consecutive slashes with a single slash character
    # url_path = re.sub('[^:](\/\/+)', '/', url_path)

    # canonical_url = '{}://{}{}{}{}'.format(parsed_url.scheme, parsed_url.netloc, url_path, parsed_url.params, parsed_url.query)
    # if parsed_url.fragment:
    #     canonical_url = canonical_url + '#{}'.format(parsed_url.fragment)
    # return canonical_url


def url_base_form(url):
    """Get the base URL without a path, query strings, or other junk."""
    parsed_url = urllib.parse.urlparse(url)
    return '{}://{}/'.format(parsed_url.scheme, parsed_url.netloc)


def url_simple_form(url):
    """Return the URL without query strings or fragments."""
    new_url = url
    new_url = url_without_query_strings(new_url)
    new_url = url_without_fragments(new_url)
    return new_url


def url_without_query_strings(url):
    """Return the URL without any query strings."""
    parsed_url = urllib.parse.urlparse(url)
    new_url = '{}://{}{}{}'.format(parsed_url.scheme, parsed_url.netloc, parsed_url.path, parsed_url.params)
    if parsed_url.fragment:
        new_url = '{}#{}'.format(new_url, parsed_url.fragment)
    return new_url


def url_without_fragments(url):
    """Return the URL without any fragments."""
    return urllib.parse.urldefrag(url).url


def host_as_punycode(host):
    """Convert the given host to Punycode (https://en.wikipedia.org/wiki/Punycode)."""
    return host.encode('idna').decode('utf-8')


def host_as_unicode(host):
    """Convert a given host to Unicode (https://en.wikipedia.org/wiki/Unicode)."""
    return host.encode('utf-8').decode('idna')


def url_as_punycode(url):
    """Convert the host of the URL to Punycode."""
    host = url_host(url)
    return url.replace(host, host_as_punycode(host), 1)


def url_as_unicode(url):
    """Convert the host of the URL to Unicode."""
    host = url_host(url)
    return url.replace(host, host_as_unicode(host), 1)


def ipv6_expand(ip_v6):
    """."""
    return ipaddress.IPv6Address(ip_v6).exploded


def ipv6_compress(ip_v6):
    """."""
    return ipaddress.IPv6Address(ip_v6).compressed


def ipv6_threatconnect_format(ip_v6):
    """."""
    address_sections = [section.replace("0000", "xxxx").lstrip("0") for section in ipv6_expand(ip_v6).split(":")]
    formatted_address_sections = ":".join(address_sections)
    return formatted_address_sections.replace("xxxx", "0")


def file_get_details(file_path):
    """Find the file details (md5, sha1, sha256, file size) for the given file."""
    file_details = dict()

    with open(file_path, 'rb') as f:
        file_text = f.read()
        file_details['md5'] = hashlib.md5(file_text).hexdigest()
        file_details['sha1'] = hashlib.sha1(file_text).hexdigest()
        file_details['sha256'] = hashlib.sha256(file_text).hexdigest()
    file_details['size'] = os.stat(file_path).st_size

    return file_details


def files_get_details(directory_path):
    """Find the file details (md5, sha1, sha256, file size) for all files in a directory."""
    file_details = dict()

    for path, dirs, files in os.walk(directory_path):
        for file_ in files:
            # TODO: handle situations where there are multiple files with the same name
            file_details[file_] = file_get_details(os.path.join(path, file_))

    return file_details
