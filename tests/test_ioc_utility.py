#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_ioc_utility
----------------------------------

Tests for `ioc_utility` module.
"""

import os

from ioc_utility import ioc_utility


def test_host_from_url():
    url = 'https://hightower.space/projects/'
    assert ioc_utility.url_host(url) == 'hightower.space'


def test_url_from_google_redirect():
    url = 'https://www.google.com/url?q=https%3A%2F%2Fwebmail.helsingor.dk%2Fowa%2Fredir.aspx%3FC%3DiN3lskTD_4ItZaDxsLe0d-3yBMbVhDqMiHJd442Po8vbP4omOITVCA..%26URL%3Dhttp%253a%252f%252fwww.google.com%252furl%253fq%253dhttp%25253A%25252F%25252Fsitesumo.com%25252Fccff%25252Fmain.html%2526sa%253dD%2526sntz%253d1%2526usg%253dAFQjCNGLOdDHK3rXm0xfJjyoo_rAFB5y6g&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNFyB3x8dfKsi2-Q21IzELCGEe_b3Q'
    assert ioc_utility.url_from_google_redirect(url) == 'https://webmail.helsingor.dk/owa/redir.aspx?C=iN3lskTD_4ItZaDxsLe0d-3yBMbVhDqMiHJd442Po8vbP4omOITVCA..&URL=http%3a%2f%2fwww.google.com%2furl%3fq%3dhttp%253A%252F%252Fsitesumo.com%252Fccff%252Fmain.html%26sa%3dD%26sntz%3d1%26usg%3dAFQjCNGLOdDHK3rXm0xfJjyoo_rAFB5y6g'


def test_url_canonical_form():
    """These tests are taken from https://developers.google.com/safe-browsing/v4/urls-hashing#canonicalization."""
    assert ioc_utility.url_canonical_form("http://host/%25%32%35") == "http://host/%25";
    assert ioc_utility.url_canonical_form("http://host/%25%32%35%25%32%35") == "http://host/%25%25";
    assert ioc_utility.url_canonical_form("http://host/%2525252525252525") == "http://host/%25";
    assert ioc_utility.url_canonical_form("http://host/asdf%25%32%35asd") == "http://host/asdf%25asd";
    assert ioc_utility.url_canonical_form("http://host/%%%25%32%35asd%%") == "http://host/%25%25%25asd%25%25";
    assert ioc_utility.url_canonical_form("http://www.google.com/") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("http://%31%36%38%2e%31%38%38%2e%39%39%2e%32%36/%2E%73%65%63%75%72%65/%77%77%77%2E%65%62%61%79%2E%63%6F%6D/") == "http://168.188.99.26/.secure/www.ebay.com/";
    assert ioc_utility.url_canonical_form("http://195.127.0.11/uploads/%20%20%20%20/.verify/.eBaysecure=updateuserdataxplimnbqmn-xplmvalidateinfoswqpcmlx=hgplmcx/") == "http://195.127.0.11/uploads/%20%20%20%20/.verify/.eBaysecure=updateuserdataxplimnbqmn-xplmvalidateinfoswqpcmlx=hgplmcx/";
    assert ioc_utility.url_canonical_form("http://host%23.com/%257Ea%2521b%2540c%2523d%2524e%25f%255E00%252611%252A22%252833%252944_55%252B") == "http://host%23.com/~a!b@c%23d$e%25f^00&11*22(33)44_55+";
    assert ioc_utility.url_canonical_form("http://3279880203/blah") == "http://195.127.0.11/blah";
    assert ioc_utility.url_canonical_form("http://www.google.com/blah/..") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("www.google.com/") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("www.google.com") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("http://www.evil.com/blah#frag") == "http://www.evil.com/blah";
    assert ioc_utility.url_canonical_form("http://www.GOOgle.com/") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("http://www.google.com.../") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("http://www.google.com/foo\tbar\rbaz\n2") == "http://www.google.com/foobarbaz2";
    assert ioc_utility.url_canonical_form("http://www.google.com/q?") == "http://www.google.com/q?";
    assert ioc_utility.url_canonical_form("http://www.google.com/q?r?") == "http://www.google.com/q?r?";
    assert ioc_utility.url_canonical_form("http://www.google.com/q?r?s") == "http://www.google.com/q?r?s";
    assert ioc_utility.url_canonical_form("http://evil.com/foo#bar#baz") == "http://evil.com/foo";
    assert ioc_utility.url_canonical_form("http://evil.com/foo;") == "http://evil.com/foo;";
    assert ioc_utility.url_canonical_form("http://evil.com/foo?bar;") == "http://evil.com/foo?bar;";
    assert ioc_utility.url_canonical_form("http://\x01\x80.com/") == "http://%01%80.com/";
    assert ioc_utility.url_canonical_form("http://notrailingslash.com") == "http://notrailingslash.com/";
    assert ioc_utility.url_canonical_form("http://www.gotaport.com:1234/") == "http://www.gotaport.com/";
    assert ioc_utility.url_canonical_form("  http://www.google.com/  ") == "http://www.google.com/";
    assert ioc_utility.url_canonical_form("http:// leadingspace.com/") == "http://%20leadingspace.com/";
    assert ioc_utility.url_canonical_form("http://%20leadingspace.com/") == "http://%20leadingspace.com/";
    assert ioc_utility.url_canonical_form("%20leadingspace.com/") == "http://%20leadingspace.com/";
    assert ioc_utility.url_canonical_form("https://www.securesite.com/") == "https://www.securesite.com/";
    assert ioc_utility.url_canonical_form("http://host.com/ab%23cd") == "http://host.com/ab%23cd";
    assert ioc_utility.url_canonical_form("http://host.com//twoslashes?more//slashes") == "http://host.com/twoslashes?more//slashes";


def test_url_base_form():
    url = "https://hightower.space/projects/"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects?s=1"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects/s=1"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects/bingo%20"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects/a/b/c"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects#testing"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/projects/#testing"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space/?test=bingo&q=t"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'
    url = "https://hightower.space?test=bingo&q=t"
    assert ioc_utility.url_base_form(url) == 'https://hightower.space/'


def test_url_without_query_strings():
    url = "https://hightower.space/projects/"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects?s=1"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects'
    url = "https://hightower.space/projects/?s=1"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects/bingo%20"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/bingo%20'
    url = "https://hightower.space/projects/a/b/c"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/a/b/c'
    url = "https://hightower.space/projects#testing"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects#testing'
    url = "https://hightower.space/projects/#testing"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/#testing'
    url = "https://hightower.space/projects/?test=bingo&q=t#test"
    assert ioc_utility.url_without_query_strings(url) == 'https://hightower.space/projects/#test'


def test_url_without_fragments():
    url = "https://hightower.space/projects/"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects?s=1"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects?s=1'
    url = "https://hightower.space/projects/?s=1"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/?s=1'
    url = "https://hightower.space/projects/bingo%20"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/bingo%20'
    url = "https://hightower.space/projects/a/b/c"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/a/b/c'
    url = "https://hightower.space/projects#testing"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects'
    url = "https://hightower.space/projects/#testing"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects/?test=bingo&q=t#test"
    assert ioc_utility.url_without_fragments(url) == 'https://hightower.space/projects/?test=bingo&q=t'


def test_url_simple_form():
    url = "https://hightower.space/projects/"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects?s=1"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects'
    url = "https://hightower.space/projects/?s=1"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects/bingo%20"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/bingo%20'
    url = "https://hightower.space/projects/a/b/c"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/a/b/c'
    url = "https://hightower.space/projects#testing"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects'
    url = "https://hightower.space/projects/#testing"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/'
    url = "https://hightower.space/projects/?test=bingo&q=t#test"
    assert ioc_utility.url_simple_form(url) == 'https://hightower.space/projects/'


def test_host_as_punycode():
    url = "☁.com"
    assert ioc_utility.host_as_punycode(url) == 'xn--l3h.com'


def test_host_as_unicode():
    url = 'xn--l3h.com'
    assert ioc_utility.host_as_unicode(url) == '☁.com'


def test_url_as_punycode():
    url = 'https://☁.com/test'
    assert ioc_utility.url_as_punycode(url) == 'https://xn--l3h.com/test'
    # make sure unicode characters in the URL path are not decoded
    url = 'https://☁.com/☁'
    assert ioc_utility.url_as_punycode(url) == 'https://xn--l3h.com/☁'


def test_url_as_unicode():
    url = 'https://xn--l3h.com/test'
    assert ioc_utility.url_as_unicode(url) == 'https://☁.com/test'


def test_ipv6_expand():
    ipv6 = '2001:db8::1000'
    assert ioc_utility.ipv6_expand(ipv6) == '2001:0db8:0000:0000:0000:0000:0000:1000'


def test_ipv6_compress():
    ipv6 = '2001:0db8:0000:0000:0000:0000:0000:1000'
    assert ioc_utility.ipv6_compress(ipv6) == '2001:db8::1000'


def test_ipv6_threatconnect_format():
    ipv6 = '2001:0db8:0000:0000:0000:0000:0000:1000'
    assert ioc_utility.ipv6_threatconnect_format(ipv6) == '2001:db8:0:0:0:0:0:1000'
    ipv6 = '2001:db8::1000'
    assert ioc_utility.ipv6_threatconnect_format(ipv6) == '2001:db8:0:0:0:0:0:1000'


def test_files_get_details():
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "./data/test_file_opps/"))
    file_details = ioc_utility.files_get_details(path)
    assert len(file_details) == 4
    assert file_details['a.txt'] == {
        'size': 2,
        'sha1': '3f786850e387550fdab836ed7e6dc881de23001b',
        'md5': '60b725f10c9c85c70d97880dfe8191b3',
        'sha256': '87428fc522803d31065e7bce3cf03fe475096631e5e07bbd7a0fde60c4cf25c7'
    }
    assert file_details['b.txt'] == {
        'size': 2,
        'sha1': '89e6c98d92887913cadf06b2adb97f26cde4849b',
        'md5': '3b5d5c3712955042212316173ccf37be',
        'sha256': '0263829989b6fd954f72baaf2fc64bc2e2f01d692d4de72986ea808f6e99813f'
    }
    assert file_details['c.txt'] == {
        'size': 2,
        'sha1': '2b66fd261ee5c6cfc8de7fa466bab600bcfe4f69',
        'md5': '2cd6ee2c70b0bde53fbe6cac3c8b8bb1',
        'sha256': 'a3a5e715f0cc574a73c3f9bebb6bc24f32ffd5b67b387244c2c909da779a1478'
    }
    assert file_details['d.txt'] == {
        'size': 2,
        'md5': 'e29311f6f1bf1af907f9ef9f44b8328b',
        'sha256': '8d74beec1be996322ad76813bafb92d40839895d6dd7ee808b17ca201eac98be', 'sha1':
        'e983f374794de9c64e3d1c1de1d490c0756eeeff'
    }
