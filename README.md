# Indicator of Compromise Utility

Helpful functions for working with indicators of compromise.

* Free software: MIT license
* Documentation: https://ioc-utility.readthedocs.io

## Usage

Coming soon...

## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter) and fhightower's [Python project template](https://github.com/fhightower-templates/python-project-template).
